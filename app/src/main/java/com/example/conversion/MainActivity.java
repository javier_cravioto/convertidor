package com.example.conversion;

import androidx.appcompat.app.AppCompatActivity;


import android.widget.Toast;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;



public class MainActivity extends AppCompatActivity {
    private TextView lblText;
    private TextView lblText2;
    private EditText lblCantidad;
    private Spinner spnConver;
    public TextView lblResultado;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnCerrar;

    private long Opcion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lblText = (TextView) findViewById(R.id.lblText);
        lblText2=(TextView) findViewById(R.id.lblText2);
        lblCantidad = (EditText) findViewById(R.id.lblCantidad);
        spnConver = (Spinner) findViewById(R.id.spnConver);
        lblResultado = (TextView) findViewById(R.id.lblResultado);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar= (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);



        final ArrayAdapter<String> Adaptador=new
                ArrayAdapter<String>(com.example.conversion.MainActivity.this,android.R.layout.simple_expandable_list_item_1,
                getResources().getStringArray(R.array.unidades));
        spnConver.setAdapter(Adaptador);
        spnConver.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Opcion=adapterView.getItemIdAtPosition(i);
                lblResultado.setText("");


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lblCantidad.setText("");
                lblResultado.setText("");
                spnConver.setAdapter(Adaptador);

            }
        });



        btnCalcular.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                if (lblCantidad.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "Faltó Capturar Número",
                            Toast.LENGTH_SHORT).show();
                }
                else {
                    float resultado=0.0f;
                    float Cantidad = Float.parseFloat(lblCantidad.getText().toString());


                    switch ((int) Opcion) {

                        case 0:
                            resultado = Cantidad * 0.05f;
                            break;
                        case 1:
                            resultado = Cantidad * 0.048f;
                            break;
                        case 2:
                            resultado = Cantidad * 0.07f;
                            break;
                        case 3:
                            resultado = Cantidad * 0.41f;
                            break;
                    }

                    lblResultado.setText(String.valueOf(resultado));


                }
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


}



